from flask import Flask
from .user import bp_user


def register_blueprints(app: Flask):
    app.register_blueprint(bp_user)
