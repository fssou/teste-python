import os
from flask import Flask


class App(Flask):
    def __init__(self, import_name):
        super().__init__(import_name)
        # the instance folder exists
        try:
            os.makedirs(self.instance_path)
        except OSError:
            pass
