from typing import List, Optional

from app.domain.model import User
from app.domain.repository import UserRepository


class UserService:
    def __init__(self, repos: UserRepository):
        self.repos = repos

    def save(self, user: User) -> Optional[User]:
        return self.repos.save(user)

    def get_all(self) -> Optional[List[User]]:
        return self.repos.find_by_all()

    def delete(self, identifier: int) -> Optional[bool]:
        self.repos.delete(identifier)
        return True
