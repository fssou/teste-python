create table usuarios
(
    id    number,
    nome  varchar(4000),
    email varchar(4000)
);

create table compras
(
    id         number,
    id_usuario number,
    valor      number,
    item       varchar(4000)
);


select u.id    id_usuario,
       u.nome  nome,
       c.id    id_compra,
       c.valor valor_compra,
       c.item  item_compra
from usuarios u
         inner join compras c
                    on u.id = c.id_usuario


select u.id    id_usuario,
       u.nome  nome,
       c.id    id_compra,
       c.valor valor_compra,
       c.item  item_compra
from usuarios u
         left join compras c
                   on u.id = c.id_usuario


