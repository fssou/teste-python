"""
1) Para monitorar o desempenho dos times em um campeonato de futebol, crie uma classe Time, que
tem como atributos o número de pontos, gols prós, gols contras e o número de partidas jogados.
Dentro desta classe, defina métodos para:

    a) calcular a média de gols por partida;
    b) calcular a média de pontos por partida;
    c) calcular o saldo de gols do time;
    d) atualizar os atributos do time de acordo com os resultados de uma partida (vitória/derrota/empate,
    gols feitos e gols sofridos)
    e) retornar os seguintes status do time: partidas jogadas, pontos feitos, saldo de gols, média de gols por
    partida, média de pontos por partida

OBS: vitória vale 3 pontos, empate vale 1 ponto e derrota não vale nada

"""
from typing import Optional


class Time:
    pontos = 0
    gols_pros = 0
    gols_contras = 0
    n_partidas_jogadas = 0

    def media_gols_por_partida(self) -> Optional[float]:
        return self.gols_pros / self.n_partidas_jogadas

    def media_pontos_por_partida(self) -> Optional[float]:
        return self.pontos / self.n_partidas_jogadas

    def saldo_de_gols(self):
        return self.gols_pros - self.gols_contras

    def resultado_partida(self, gols_pros: int, gols_contras: int) -> None:
        s_gols = gols_pros - gols_contras
        if s_gols == 0:
            self.pontos += 1
        elif s_gols > 0:
            self.pontos += 3
        self.gols_pros += gols_pros
        self.gols_contras += gols_contras
        self.n_partidas_jogadas += 1

    def status(self):
        return f"""
                    partidas jogadas: #{self.n_partidas_jogadas}, 
                    pontos feitos: #{self.pontos}, 
                    saldo de gols: #{self.saldo_de_gols()}, 
                    média de gols por partida: #{self.media_gols_por_partida()}, 
                    média de pontos por partida: #{self.media_pontos_por_partida()}
                """

